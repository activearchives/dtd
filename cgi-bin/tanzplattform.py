from __future__ import print_function
from urlparse import urljoin, urlparse
import os, re, requests
from sys import stderr
from utils import wiki_login, innerHTML, fill_template, patch_template
from scraper import Scraper


def sniff (scraper):
	if scraper.url.startswith("http://www.tanzplattform.de/typo3/kuenstlerseiten/"):
		return {
			'data': scrape(scraper),
			'template_name': "Artist page (Tanzplattform)",
			'template': artist_page_template
		}

def label (href):
    p = urlparse(href)
    ret = os.path.basename(p.path.rstrip("/")).replace("-", " ")
    return " ".join([x.capitalize() for x in ret.split()])	

artist_page_template_pat = re.compile(ur"\{\{Artist page \(Tanzplattform\).+?\}\}", re.DOTALL)

artist_page_template = u"""{{{{Artist page (Tanzplattform)
|TP URL={0[url]}
|TP Page EN={0[page_en]}
|TP Weblink artist={0[weblink_artist]}
|TP Statement DE={0[statement_de]}
|TP Statement EN={0[statement_en]}
|TP Bio DE={0[bio_de]}
|TP Bio EN={0[bio_en]}
|TP Portrait DE={0[portrait_de]}
|TP Portrait EN={0[portrait_en]}
}}}}
"""

def artists ():
	url = "http://www.tanzplattform.de/typo3/kuenstler-uebersicht/"
	wiki = wiki_login()
	page = Scraper(url)
	for href in page.links():
	    if href.startswith("http://www.tanzplattform.de/typo3/kuenstlerseiten/"):
	    	yield href

def create_artist_page (href, clear=False, patch=True):
	artist_page = Scraper(href)
	data = scrape(artist_page)
	data['url'] = href
	l = label(href)
	wikipage = wiki.Pages[l]
	if clear:
		text = fill_template(artist_page_template, data)
		wikipage.save(text)
	elif patch or not wikipage.exists:
		text = u"""[[URL::{0}]]\n""".format(href)
		text = wikipage.text()
		text = patch_template(text, artist_page_template_pat, fill_template(artist_page_template, data))
		print (text)
		print()
		wikipage.save(text)
	else:
		if wikipage.exists:
			print (u"Page '{0}' exists, skipping (use --patch to update the page)".format(l).encode("utf-8"), file=stderr)

def _jstrip (things):
	return u"".join(things).strip()

def scrape(page):
	data = {'url': page.url}
	data['statement_de'] = _jstrip(page.scrape_all(".//*[@id='statement']//p[@class='bodytext']", innerHTML))
	data['portrait_de'] = _jstrip(page.scrape_all(".//*[@id='textbox_03']//p[@class='bodytext']", innerHTML))
	data['bio_de'] = _jstrip(page.scrape_all(".//*[@id='textbox_05']//p[@class='bodytext']", innerHTML))
	data['page_en'] = page.scrape(".//a[@id='en']", page.abs_getter("href"))

	data['weblink_artist'] = page.scrape(".//*[@id='weblink_artist']//a[@href]", page.abs_getter("href"))

	if data['page_en']:
		page_en = Scraper(data['page_en'])
		data['statement_en'] = _jstrip(page_en.scrape_all(".//*[@id='statement']//p[@class='bodytext']", innerHTML))
		data['portrait_en'] = _jstrip(page_en.scrape_all(".//*[@id='textbox_03']//p[@class='bodytext']", innerHTML))
		data['bio_en'] = _jstrip(page_en.scrape_all(".//*[@id='textbox_05']//p[@class='bodytext']", innerHTML))

	data['images'] = list(page.images(filter=lambda x: x not in [u"http://www.tanzplattform.de/typo3/uploads/pics/tanzplattform_header_neu.png"]))
	return data


if __name__ == "__main__":
	from argparse import ArgumentParser
	from pprint import pprint
	from time import sleep

	p = ArgumentParser(description="Tanzplattorm Spider")
	p.add_argument("command", help="Command to perform")
	p.add_argument("--clear", action="store_true", help="Reset text to just the template")
	p.add_argument("--patch", action="store_true")
	p.add_argument("--url", default="http://www.tanzplattform.de/typo3/kuenstlerseiten/wilhelm-groener/")

	args = p.parse_args()

	if args.command == "artists":
		for href in artists():
			print(href)

	elif args.command == "scrape":
		page = Scraper(args.url)
		pprint(scrape(page))

	else:
		print ("Command not understood")