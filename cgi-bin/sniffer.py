from scraper import Scraper
import tanzplattform, mimecentrum
import re
from utils import wiki_login


sniffers = [tanzplattform.sniff, mimecentrum.sniff]

def sniff (url):
	scraper = Scraper(url)
	ret = []
	for sniffer in sniffers:
		data = sniffer(scraper)
		if data:
			if type(data) == list or type(data) == tuple:
				ret.extend(data)
			else:
				ret.append(data)
	return ret

def wiki_title_for_url (url):
	ret = re.sub(r"https?\:\/\/", "", url)
	return ret

def wiki_link (page):
	return "http://"+page.site.host+'/w/'+page.normalize_title(page.name)

def fill_template(template, data):
	d2 = {}
	# TODO: Parse previous values and allow these to override empty values
	# (to allow an empty template / values to force the template but not overwrite editors values)
	for key, value in data.items():
		d2[key] = value or ""
	return template.format(d2)

def patch_template (text, template_name, filled_template):
	pat = re.compile(re.escape("{{"+template_name)+".*?"+re.escape("}}"), re.DOTALL)
	m = pat.search(text)
	if m != None:
		return pat.sub(text, filled_template, count=1)
	else:
		return text+u"\n"+filled_template

def create_or_update_wikipage_for (url, dryrun=False, recursive=False, forceUpdate=False):
	title = wiki_title_for_url(url)
	wiki = wiki_login()
	wikipage = wiki.Pages[title]
	wikitext = wikipage.text()
	for r in sniff(url):
		filled = fill_template(r['template'], r['data'])
		wikitext = patch_template(wikitext, r['template_name'], filled)
		if not dryrun:
			wikipage.save(wikitext)
	return wikipage, wikitext

if __name__ == "__main__":
	import sys
	from pprint import pprint
	for url in sys.argv[1:]:
		print url
		for resp in sniff(url):
			print resp['template'].format(resp['data'])
		# pprint (sniff(url))
		print

