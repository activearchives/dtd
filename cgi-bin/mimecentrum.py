from utils import wiki_login, innerHTML, fill_template, patch_template
from requests import get, post	
import requests, re
from operator import attrgetter
from argparse import ArgumentParser
from scraper import Scraper

search_url = "http://archiv.mimecentrum.de/search/do_search"

# def search(term):
# r = requests.post(search_url, data={'phrase': search_text, 'commit': 'search'})
# page = SpiderPage(r)
# ### #listing a.plain
# print "results"
# for href in page.scrape_all(".//a[@class='plain']", accessor=page.abs_getter("href")):
# 	print href
# print
# ### continue links: a.linked_page
# print "continue links"
# for href in page.scrape_all(".//a[@class='linked_page']", accessor=page.abs_getter("href")):
# 	print href

def sniff (scraper):
	if scraper.url.startswith("http://archiv.mimecentrum.de/person/"):
		return {
			'data': scrape(scraper),
			'template_name': 'Person page (Mimecentrum)',
			'template': template
		}

def search_for_person (name):
	r = post(search_url, data={'phrase': name, 'commit': 'search'})
	page = Scraper(search_url, request=r)
	for l in page.links():
		if l.startswith("http://archiv.mimecentrum.de/person/"):	
			return l

tail = attrgetter('tail')

template_pat = re.compile(ur"\{\{Person page \(Mimecentrum\).+?\}\}", re.DOTALL)

template = u"""{{{{Person page (Mimecentrum)
|MC URL={0[url]}
|MC First name={0[first_name]}
|MC Last name={0[last_name]}
|MC Birth date={0[birth_date]}
|MC Description={0[description]}
|MC Website={0[website]}
}}}}"""

def scrape(page):
	data = {'url': page.url}
	data['first_name'] = page.scrape(".//label[@for='firstname']", tail).strip()
	data['last_name'] = page.scrape(".//label[@for='lastname']", tail).strip()
	data['birth_date'] = page.scrape(".//label[@for='birthdate']", tail).strip()
	data['description'] = page.scrape(".//label[@for='description']", tail).strip()
	data['website'] = page.scrape(".//div[@id='item_show']//a", lambda x: x.get("href"))
	if data['website'] == "http://":
		data['website'] = None
	else:
		data['website'] = page.abs_url(data['website'])

	links = []
	data['links'] = links
	for span in page.scrape_all(".//div[@id='item_hints']//span[@class='addition']"):
		name = span.find(".//span[@class='icon']").tail.strip()
		a = span.find(".//a")
		link = page.abs_url(a.get("href"))
		label = innerHTML(a)
		links.append({'link': link, 'label':label,'name':name})
	return data

# print json.dumps(data)

# <form action="/search/do_search" method="post">
#   <input class="side_search" id="phrase" name="phrase" size="10" type="text"><br>
#   <input class="search_button" name="commit" value="search" type="submit">
#   </form>

if __name__ == "__main__":
	p = ArgumentParser(description="Mimecentrum Spider")
	p.add_argument("command", help="Command to perform")
	p.add_argument("--clear", action="store_true", help="Reset text to just the template")
	p.add_argument("--patch", action="store_true")
	p.add_argument("--url", default="http://archiv.mimecentrum.de/person/show/366", help="URL to scrape")
	args = p.parse_args()

	if args.command == "artists":
		wiki = wiki_login()
		cat = wiki.Pages['Category:Artist page (Tanzplattform)']
		for wikipage in cat:
			mc_url = search_for_person(wikipage.name)
			if mc_url:	
				data = scrape(Scraper(mc_url))	
				data['url'] = mc_url
				text = wikipage.text()
				newtext = patch_template(text, template_pat, fill_template(template, data))
				print wikipage.name
				# print "NEWTEXT"
				# print newtext
				# break
				wikipage.save(newtext)

	elif args.command == 'scrape':
		from pprint import pprint
		pprint(scrape(Scraper(args.url)))
