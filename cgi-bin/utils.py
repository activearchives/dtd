# from urllib2 import urlopen
# -*- coding: utf-8 -*-
from requests import get
from html5lib import parse
from urlparse import urljoin
from settings import *
from secret import *
from xml.etree import ElementTree as ET
import re


def wiki_login ():
	wiki = Site(WIKI_HOST, path=WIKI_PATH)
	wiki.login("bot", BOT_PASSWORD)
	return wiki

def fill_template(template, data):
	d2 = {}
	for key, value in data.items():
		d2[key] = value or ""
	return template.format(d2)

def patch_template (text, template_name, filled_template):
	pat = re.compile(re.escape("{{"+template_name)+".*?"+re.escape("}}"), re.DOTALL)
	m = pat.search(text)
	if m != None:
		return pat.sub(text, filled_template, count=1)
	else:
		return text+u"\n"+filled_template

def innerHTML (elt, strip=True):
	ret = (elt.text or u'')
	ret += u''.join([ET.tostring(x) for x in elt])
	if strip:
		ret = ret.strip()
	return ret

if __name__ == "__main__":
	text = u"""
{{Person page (Mimecentrum)
|MC URL=http://archiv.mimecentrum.de/person/show/366
|MC First name=Sasha
|MC Last name=Waltz
|MC Birth date=1963-03-08
|MC Description=Sasha Waltz (* 8. März 1963 in Karlsruhe) ist eine deutsche Choreografin, Tänzerin und Leiterin eines Tanzensembles.
 VITA: Sasha Alexandra Waltz ist die Tochter eines Architekten und einer Galeristin. Sie bekam mit fünf Jahren ihren ersten Tanzunterricht bei der Mary-Wigman-Schülerin Waltraud Kornhaas in Karlsruhe. Von 1983 bis 1986 studierte Waltz an der School For New Dance Development in Amsterdam.

Daran schloss sich eine Weiterbildung in New York von 1986 bis 1987 an. Dort war sie als Tänzerin in den Compagnien von Pooh Kaye, Yoshiko Chuma & School Of Hard Knocks sowie Lisa Kraus & Dancers engagiert. Es folgte eine intensive Zusammenarbeit mit Choreographen, Bildenden Künstlern und Musikern wie Tristan Honsinger, Frans Poelstra, Mark Tompkins, David Zambrano u.v.a.

Von 1992 an war sie artist in residence im Künstlerhaus Bethanien. Hier entwickelte sie ihre Reihe "Dialoge" von interdisziplinären Projekten mit Tänzern, Musikern und Bildenden Künstlern. Die Gründung der Compagnie Sasha Waltz & Guests erfolgte gemeinsam mit Jochen Sandig im Jahre 1993. Im Laufe der folgenden drei Jahre entstand die "Travelogue-Trilogie".

Gemeinsam mit Jochen Sandig, gründete sie 1996 die Sophiensæle in Berlin-Mitte, die sich zu einer der wichtigsten Produktionsstätten für freies Theater und Tanz in Europa entwickelten. Hier entstanden "Allee der Kosmonauten" (1996), "Zweiland" (1997) und "Na Zemlje" (1998), sowie das Projekt "Dialoge `99/I".

1999 übernahm sie die Künstlerische Leitung der Schaubühne am Lehniner Platz, Berlin, gemeinsam mit Thomas Ostermeier, Jens Hillje und Jochen Sandig. Sie eröffnete die Schaubühne unter neuer Leitung mit der Uraufführung von "Körper" (2000). Die Produktion wurde zum 38. Theatertreffen eingeladen. Es folgten "S" (2000), "noBody" (2002), "insideout" (2003) , "Impromptus" (2004) und "Gezeiten" (2005).

Nach Ablauf des fünfjährigen Vertrags machte sie sich ab 2005 wieder selbständig und reaktivierte Sasha Waltz & Guests als internationales Kulturunternehmen mit 25 festen und 40 freien Mitarbeitern mit Sitz in Berlin. "Dido & Aeneas" von Henry Purcell in Koproduktion mit der Akademie für Alte Musik feierte 2005 im Grand Théâtre de la Ville de Luxemburg Premiere und entwickelte sich zu einer der international erfolgreichsten Opernproduktionen mit bisher 40 Vorstellungen in 12 Ländern.

Sasha Waltz ist gemeinsam mit dem Rechtsanwalt Stefan Mumme seit 2007 Vorstand der gemeinnützigen „Radial Stiftung“ mit Sitz im Radialsystem V - New Space for the Arts in Berlin. Die Initiative zu diesem utopischen Raum für die Künste geht auf ihren Partner Jochen Sandig und den Kulturmanager Folkert Uhde zurück, die beide geschäftsführende Gesellschafter der Radialsystem V GmbH und Künstlerischen Leiter der Einrichtung sind. Das Radialsystem ist privatwirtschaftlich als GmbH organisiert und liegt im Herzen der drei Innenstadtbezirke Berlin-Mitte, Friedrichshain und Kreuzberg in unmittelbarer Nähe des Ostbahnhofs am Ufer der Spree. Das ehemalige Pumpwerk ist ein denkmalgeschütztes Industriedenkmal und wurde nach Plänen des Architekten Gerhard Spangenberg mit privaten Mitteln zu einer interdisziplinären Produktions- und Spielstätte ausgebaut, die für die Produktion und Präsentation einer Vielzahl von internationalen künstlerischen Projekten offensteht. Der Dialog zwischen Kultur und Wirtschaft, Klassik und Pop, Tradition und Moderne ist Programm und wird in der Praxis modellhaft umgesetzt. Das Radialsystem versteht sich dabei als Katalysator der in Berlin dynamisch anwachsenden Kreativindustrie.

Sasha Waltz & Guests wird vom Land Berlin mit einem eigenen Haushaltstitel institutionell gefördert, erwirtschaftet jedoch fast die Hälfte seines Etats selbst durch Gastspiele und internationale Koproduktionen.

Mit dem „Solo für Vladimir Malakhov“ stellten die beiden Künstler am 20. April 2006 im Haus der Kulturen der Welt ihr erstes gemeinsames Projekt auf dem Deutschen Tanzkongress in Berlin vor.

Am 16. September 2007 fand in der Staatsoper Unter den Linden Berlin die Deutschlandpremiere der Opernchoreographie "Medea" statt, ein musikalisches Werk des französischen Komponisten Pascal Dusapin auf der Grundlage des Textes "medeamaterial" von Heiner Müller.

Am 5. Oktober 2007 fand die Uraufführung ihrer Choreographie "Romeo et Juliette" von Hector Berlioz an der Opéra de Bastille, Paris statt. Die musikalische Leitung dieser Produktion übernahm Waleri Gergiew.

Zum 15-jährigen Bestehen von Sasha Waltz & Guests im Jahre 2008 ist ein umfangreicher Bildband mit dem Titel "Cluster" im Henschel Verlag erschienen.

Für die nachhaltige Sicherung einer zukünftigen Choreographengeneration mit Hilfe des Förderprogramms „Choreographen der Zukunft“, konnte die BASF als Hauptsponsor gewonnen werden. Sasha Waltz ist Schirmherrin von „TanzZeit- Zeit für Tanz in den Schulen“ und leitet die Kindertanzcompany Berlin.

Sasha Waltz und Jochen Sandig haben einen Sohn László (* 5. August 1997) und eine Tochter Sophia (*27. Dezember 2002).
|MC Website=
}}
"""

	print patch_template(text, u"Person page (Mimecentrum)", "*").encode("utf-8")
