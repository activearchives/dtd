#!/usr/bin/env python

import cgitb; cgitb.enable()
import os, sys, cgi, re
from sniffer import create_or_update_wikipage_for, wiki_link


method = os.environ.get("REQUEST_METHOD")
if method == "POST":
	fs = cgi.FieldStorage()
	url = fs.getvalue("url", "").strip()
	if url:
		dryrun = fs.getvalue("dryrun", False)
		wikipage, wikitext = create_or_update_wikipage_for(url, dryrun=dryrun)
		if dryrun:
			print "Content-type: text/plain;charset=utf-8"
			print
			wikilink = wiki_link(wikipage)
			print wikilink
			print "="*80
			print wikitext.encode("utf-8")
		elif wikipage:
			wikilink = wiki_link(wikipage)
			print "Location: {0}".format(wikilink)
			print
			print """<meta http-equiv="refresh" content="0; url={0}">""".format(wikilink)
		else:
			print "Content-type: text/plain"
			print
			print "An error occured."
		sys.exit(0)

print "Content-type:text/html;charset=utf-8"
print
print """<h1>Add URL</h1>
<form method="post" action="">
<input type="input" name="url" style="width: 80%" autofocus placeholder="URL" value="" />
<input type="checkbox" id="dryrun" name="dryrun" value="dryrun" /><label for="dryrun">dry run</label>
<input type="submit" name="_submit" value="ok" />
</form>
"""
