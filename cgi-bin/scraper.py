from requests import get
from html5lib import parse
from urlparse import urljoin
from xml.etree import ElementTree as ET


class Scraper (object):

	def __init__(self, url, request=None, get=True):
		self.url = url
		if request:
			set_request(request)
		elif get:
			self.get()

	def get (self):
		self.set_request(get(self.url))

	def set_request (self, r):
		self.request = r
		self.content_type = r.headers['content-type']
		self.url = r.url
		t = parse(r.text, namespaceHTMLElements=False)
		ret = {}
		self.tree = t
		self.base = self.url
		# check for an explicit base element
		base = t.find(".//base")
		if base != None:
			self.base = base.get("href")
		return self

	def abs_url (self, href):
		return urljoin(self.base, href)

	def abs_getter (self, attrname="href"):
		def _getter (elt):
			return self.abs_url(elt.get(attrname))
		return _getter

	def scrape (self, selector, accessor=None):
		elt = self.tree.find(selector)
		if elt != None:
			if accessor == None:
				return elt
			else:
				return accessor(elt)

	def scrape_all (self, selector, accessor=None, filter=None):
		for elt in self.tree.findall(selector):
			if accessor:
				ret = accessor(elt)
				if filter != None:
					if not filter(ret):
						continue
				if ret != None:
					yield ret
			else:
				if filter != None:
					if not filter(elt):
						continue
				yield elt

	def links (self):
		""" Iterator over absolute href's of a elements """
		return self.scrape_all(".//a", self.abs_getter())

	def images (self, filter=None):
		""" Iterator over absolute href's of a elements """
		return self.scrape_all(".//img", accessor=self.abs_getter("src"), filter=filter)

def innerHTML (elt, strip=True):
	ret = (elt.text or u'')
	ret += u''.join([ET.tostring(x) for x in elt])
	if strip:
		ret = ret.strip()
	return ret
